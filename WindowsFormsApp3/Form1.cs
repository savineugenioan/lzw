﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace WindowsFormsApp3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Stopwatch stopwatch = new Stopwatch();
            //stopwatch.Start();
            OpenFileDialog x = new OpenFileDialog();
            DialogResult dr = x.ShowDialog();
            string text=string.Empty,curent=string.Empty,output=string.Empty;
            if (dr == DialogResult.OK)
            {
                StreamReader sr = new StreamReader(x.FileName);
                text = sr.ReadToEnd();
                sr.Close();
            }
            else
            {
                return;
            }
            string timer = x.FileName;
            timer = timer.Remove(timer.LastIndexOf('.'));
            Dictionary<string, int> dictionar = new Dictionary<string, int>();
            for (int i = 0; i < 256; i++)
                dictionar.Add(((char)i).ToString(), i);
            if (File.Exists(timer +"_comprimat.txt") == true)
                File.Delete(timer+"_comprimat.txt");
            int afis = 1;
            progressBar1.Minimum = 0;
            progressBar1.Maximum = text.Length;
            progressBar1.Value = 0;
            textBox1.Text = "";
            foreach (char next in text)
                try
            {
                progressBar1.Value++;
                textBox1.AppendText(afis++ +"/"+ text.Length+ Environment.NewLine);
                curent = output + next;
                if (dictionar.ContainsKey(curent))
                {
                    output = curent;
                }
                else
                {
                    File.AppendAllText(timer + "_comprimat.txt", dictionar[output] + " ");
                    //File.AppendAllText("dictionar_c.txt", curent + Environment.NewLine);
                    dictionar.Add(curent, dictionar.Count);
                    output = next.ToString();
                }
                
            }
              catch(Exception)
               {
                   MessageBox.Show("Textul contine caractere in afara codului ASCII !"+ Environment.NewLine + "Caracterul de la care a provenit eroarea : "+ output,"Eroare Caracter", MessageBoxButtons.OK, MessageBoxIcon.Error);
                   progressBar1.Value = 0;
                    
                    return;
               }
            if (!string.IsNullOrEmpty(output))
                File.AppendAllText(timer + "_comprimat.txt", dictionar[output] + " ");
            progressBar1.Value = 0;
            //textBox1.Text = "";
            MessageBox.Show("Compresare terminata");
            //stopwatch.Stop();
            //File.AppendAllText(timer +"_comp_times.txt",stopwatch.Elapsed + Environment.NewLine);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Stopwatch stopwatch = new Stopwatch();
            //stopwatch.Start();
            OpenFileDialog x = new OpenFileDialog();
            DialogResult dr = x.ShowDialog();
            string output = string.Empty,curent = string.Empty;
            int[] v= { };
            int n = 0;
            if (dr == DialogResult.OK)
                try
                {
                StreamReader sr = new StreamReader(x.FileName);
                char[] charSeparators = new char[] { ' ' };
                string[] text = sr.ReadToEnd().Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);
                sr.Close();
                n = text.Count();
                v = new int[n];
                for(int i=0;i<n;i++)
                    v[i] = int.Parse(text[i]);
                }
                catch (FormatException/* error*/)
                {
                    MessageBox.Show("Formatul textului este neadecvat ! Este posibil sa fi ales fisierul gresit pentru decompresare.","Eroare", MessageBoxButtons.OK,MessageBoxIcon.Error);
                    return;
                }
            else
            {
                return;
            }
            string timer = x.FileName;
            timer = timer.Remove(timer.LastIndexOf('.'));
            Dictionary<int, string> dictionar = new Dictionary<int, string>();
            for (int i = 0; i < 256; i++)
                dictionar.Add(i,((char)i).ToString());
            if (File.Exists(timer + "_necomprimat.txt") == true)
                File.Delete(timer +"_necomprimat.txt");
            curent = dictionar[v[0]];
            File.AppendAllText(timer + "_necomprimat.txt", curent);
            progressBar1.Minimum = 0;
            progressBar1.Maximum = n;
            progressBar1.Value = 0;
            textBox1.Text = "";
            for (int i= 1;i < n;i++)
            {
                output = null;
                progressBar1.Value++;
                textBox1.AppendText(i + "/" + n + Environment.NewLine);
                if (dictionar.ContainsKey(v[i]))
                    output = dictionar[v[i]];
                else if (v[i] == dictionar.Count)
                    output = curent + curent[0];

                File.AppendAllText(timer + "_necomprimat.txt",output);
                dictionar.Add(dictionar.Count, curent + output[0]);
                //File.AppendAllText("dictionar.txt", curent + output[0]+Environment.NewLine);
                curent = output;
            }
            textBox1.AppendText(n + "/" + n + "\n");
            progressBar1.Value = 0;
            //stopwatch.Stop();
            //File.AppendAllText(timer+"_decomp_times.txt", stopwatch.Elapsed + Environment.NewLine);
            MessageBox.Show("Decompresare terminata");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
